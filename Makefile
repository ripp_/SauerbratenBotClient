CC=g++
CFLAGS=-O2 -g0 -std=gnu++11 -pedantic -Wall -Wextra
LDFLAGS=-lm -lenet -lconfig++
INCLUDES=-I/usr/local/include -I.

BinaryName=sauerbot.app


OBJS=main.o handledata.o create.o bot.o
all: $(OBJS) $(BinaryName)


%.o:        %.cpp
	$(CC) $(CFLAGS) $(INCLUDES) -c $<

$(BinaryName):
	$(CC) -o $(BinaryName) $(CFLAGS) $(LDFLAGS) $(INCLUDES) $(OBJS)

clean:
	rm -rf *.o $(BinaryName)
