#ifndef HANDLER_H
#define HANDLER_H

void datahandler(char *,long, ENetPeer *);
int getint(char *,int&);
char getbyte(char *,int&);
const char* getstring(char *,int&);
#endif