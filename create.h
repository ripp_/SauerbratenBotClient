#ifndef CREATE_H
#define CREATE_H

void createpacket(ENetPeer *connpeer, const char* fmt...);
void sendpacket(ENetPeer *connpeer,ENetPacket *packet);

#endif