#ifndef BOT_H
#define BOT_H

#include <string>
#include <enet/enet.h>


class Bot {

	public:
		Bot(char *);
	        ~Bot();
		int host_create();
		void event_loop();
		void host_clearup();

	private:
		std::string name;
		std::string server;
		unsigned int port;
		
		ENetHost *clienthost = NULL;
		ENetPeer *connpeer = NULL;
		ENetAddress address;
		ENetEvent event;
};



#endif
