#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <string>
#include <cstring>
#include <inttypes.h>
#include <signal.h>
#include <enet/enet.h>
#include "debug.h"
#include "create.h"
#include "handler.h"
#include "sauer.h"

void datahandler(char *data, long datalength, ENetPeer *connpeer){

	int off = 0;
	int messagecode = getint(data,off);
	bool welcomeMessage = false;

	switch(messagecode){
		//Server and Map related messages
		case N_SERVINFO:{
			std::cout<< "Server Info" << std::endl;
			int mycn = getint(data,off);
			int prot = getint(data,off);
			int sessId = getint(data,off);
			bool passworded = getint(data,off)>0;
			const char* info = getstring(data,off);
			const char* auth = getstring(data,off);
			std::cout << "Connected" << std::endl << " clientNumber:" << mycn << " protocol:" << prot << std::endl;
			std::cout << "  sessionId:" << sessId << " passworded " << passworded << std::endl;
			std::cout << "  " << info << ". AuthInfo:" << auth << std::endl;

			//Reply with an N_CONNECT to the SERVER, and also make ourselves a spectator
			createpacket(connpeer, "dsdsss",N_CONNECT,"li'Botler",1,"","","");
			createpacket(connpeer, "ddd",N_SPECTATOR,mycn,1);
			break;
		}

		case N_WELCOME:{
			//Has no payload of it's own but contains several other packets as it's payload
			//They seem to be (in order): MAPCHANGE, TIMEUP, ITEMLIST, SPAWNSTATE,
			//INITCLIENT (for each client). 0xFF seems to be used to end lists for spawnstate and itemlist...
			welcomeMessage = true;
			std::cout << "== WELCOME DUMP ==" << std::endl;
			std::cout << "  == RAW DATA ==" << std::endl;
			for(int i = 0; i < datalength; i++){
				std::cout << (int)(unsigned char)data[i] << " ";
			}
			std::cout << std::endl << "  == END  RAW ==" << std::endl;

		}
		case N_MAPCHANGE:{
			const char* newmap = getstring(data,off);
			int mode = getint(data,off), u2 = getint(data,off);
			std::cout << "Map changed to " << newmap << " on mode " << mode << std::endl;
			if (!welcomeMessage || getint(data,off) != N_TIMEUP) break;
		}
		case N_TIMEUP:{
			int timeLeft = getint(data,off);
			std::cout << timeLeft << "Seconds left on current map" << std::endl;
			if (!welcomeMessage || getint(data,off) != N_ITEMLIST) break;
		}
		case N_ITEMLIST:{
			if (!welcomeMessage) break;
			
			std::cout << "Itemlist" << std::endl;

			while (getint(data,off) != -1){}// Cheat here till I get this one implemented
			if (getint(data,off) != N_SETTEAM) break;
		}
		case N_SETTEAM:{
			int wn;
			while (((wn = getint(data,off)) != -1) && off < datalength){
				const char* team = getstring(data,off);
				if (!welcomeMessage) int reason = getint(data,off);
				std::cout << wn << " changed to team " << team << std::endl;
			}
			if (getint(data,off) != N_SPAWNSTATE) break;
		}
		case N_SPAWNSTATE:{
			if (!welcomeMessage) break;
			
			while (getint(data,off) != -1 && off < datalength){}// Cheat here till I get this one implemented
			if (getint(data,off) != N_INITCLIENT) break;
		}
		case N_INITCLIENT:{ //Also thrown for user changing name
			int cn;
			while ((cn = getint(data,off)) != -1 && off < datalength){
				getstring(data,off);
				getstring(data,off);
				getint(data,off);
				const char* name = getstring(data,off);
				const char* team = getstring(data,off);
				int model = getint(data,off);
				std::cout << name << " joined the game with client number " << cn << " on team " << team << " with model " << model << std::endl;
				if (!welcomeMessage) break;
			}
			if (welcomeMessage){
				std::cout << "=== END DUMP ===" << std::endl;
			}
			break;
		}



		//General Player Interation mEssages
		case N_DIED:{
			int vcn = getint(data,off), acn = getint(data,off), frags = getint(data,off), tfrags = getint(data,off);
			std::cout << (int)(unsigned char)acn << " Killed " << (int)(unsigned char)vcn << ". Frags, " << frags << " tfrags? " << tfrags << std::endl;
			break;
		}


		//CTF Messages
		case N_DROPFLAG:{
			int ocn = getint(data,off), flag = getint(data,off), version = getint(data,off);
			int loc[3] = {getint(data,off),getint(data,off),getint(data,off)};
			std::cout << (int)(unsigned char)ocn << " Droped flag " << flag << "." << version
			<< " at " << loc[0] << "," << loc[1] << "," << loc[2] << std::endl;
			break;
		}
		case N_SCOREFLAG:{
			int ocn = (int)(unsigned char)getint(data,off),
				relayflag = getint(data,off), relayversion = getint(data,off),
				goalflag = getint(data,off), goalversion = getint(data,off), goalspawn = getint(data,off),
				team = getint(data,off), score = getint(data,off), oflags = getint(data,off);

			std::cout << ocn << " Captured flag " << relayflag << "." << relayversion
			<< " Bringing it to " << goalflag << "." << goalversion << " at " << goalspawn
			<< " for team " << team << " Scoring " << score << ". oflags? " << oflags << std::endl;
		}

		//Ignore form loggin
		case N_CLIENT:
		case N_SHOTFX: //Sound effect from gun firing, mabe can be sued to work out where and in what directions guns are fired
		case N_HITPUSH:
		{break;} 

		default:{
			std::cout << "               Unhandled Packet:" << N_REVERSE_DEBUG[(unsigned int)data[0]] << " len:" << std::dec << datalength << std::endl;
			/*for(int i = 0; i < datalength; i++){
				std::cout << (int)(unsigned char)data[i] << " ";
			}
			std::cout << std::endl;*/
			break;
		}
		break;
	}
}

char getbyte(char *data,int& off){
	return data[off++];
}

int getint(char *data,int& off){
    int c = getbyte(data,off);
    if(c==-128) {
    	int n = getbyte(data,off);
    	n |= getbyte(data,off)<<8;
    	return n;
    } else if(c==-127) {
    	int n = getbyte(data,off);
    	n |= getbyte(data,off)<<8;
    	n |= getbyte(data,off)<<16;
    	return n|(getbyte(data,off))<<24;
    } else {
    	return c;
    }
}

/*
char* getstring(char *data,int& off){
	//int len = strlen(data+off);
	int i;
	char c;
	char out[256];
	for(i=0;(c = *(data+off+i)) != '\x00';i++){
		out[i] = c;
	}
	out[++i] = '\x00';
	off+=i;
	return out;
}*/

const char* getstring(char *data,int& off){
	std::string out(&data[off]);
	off += out.length()+1;
	const char* out2 = out.c_str();
	return out2;
}

