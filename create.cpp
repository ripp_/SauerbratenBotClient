#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>
#include <vector>
#include <inttypes.h>
#include <signal.h>
#include <enet/enet.h>
#include <cstdarg>
#include "create.h"
#include "handler.h"
#include "sauer.h"
#include "bot.h"

std::string putint(int n){
	if(n < 128 && n > -127){
		std::string output(1, n);
		return(output);
	} else if (n < 0x8000 && n >= -0x8000) {
		char t[3];
		t[0] = 0x80;
		t[1] = n & 0xFF;
		t[2] = n >> 8;
		std::string output(t,3);
		return(output);
	} else {
		char t[5];
		t[0] = 0x80;
		t[1] = n & 0xFF;
		t[2] = (n >> 8) & 0xFF;
		t[3] = (n >> 16) & 0xFF;
		t[4] = n >> 24;
		std::string output(t,5);
		return(output);
	}
}

void createpacket(ENetPeer *connpeer, const char* fmt...){

	std::vector<char> argstring;

	va_list args;
	va_start(args, fmt);

	while (*fmt != '\0') {
		if (*fmt == 'd') {
			int i = va_arg(args, int);
			std::string weirdstring = putint(i);
			for(unsigned int i = 0; i < weirdstring.size(); i++){
				argstring.push_back(weirdstring[i]);
			}
		} else if (*fmt == 's') {
			char *string_temp = va_arg(args, char *);
			for(unsigned int i = 0; i < std::strlen(string_temp); i++){
				argstring.push_back(string_temp[i]);
			}
			argstring.push_back('\0');
		}
		++fmt;
	}


	std::string packetstring;
	std::cout << std::hex;
	for(unsigned int i = 0; i < argstring.size(); i++){
		packetstring.push_back(argstring.at(i));
		std::cout << (int)packetstring[i];
	}



	std::cout << std::endl << std::dec;

	std::cerr << "Just Sent Packet of Length: " << packetstring.size() << std::endl;
	ENetPacket *packet = enet_packet_create(packetstring.c_str(),argstring.size(), ENET_PACKET_FLAG_RELIABLE);
	
	va_end(args);

	sendpacket(connpeer,packet);
}


void sendpacket(ENetPeer *connpeer,ENetPacket *packet){

	enet_peer_send (connpeer, 1, packet);

}
