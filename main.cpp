#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>
#include <inttypes.h>
#include <signal.h>
#include <enet/enet.h>
#include <libconfig.h++>
#include "create.h"
#include "handler.h"
#include "sauer.h"
#include "bot.h"

void deinitialise(int s){
	int signal = s;
	std::cout << "Caught signal: " << signal << std::endl;
	exit(1);
}

int main(int argc, char **argv) {
	//Enet Initilize
	if (enet_initialize () != 0) {
		std::cerr << "An error occurred while initializing ENet." << std::endl;
		return EXIT_FAILURE;
	}
	atexit (enet_deinitialize);

	// Catch sigint
	struct sigaction sigIntHandler;

	sigIntHandler.sa_handler = deinitialise;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	sigaction(SIGINT, &sigIntHandler, NULL);


	// Create Bot Class that does clever things
	if(argc != 2){
		std::cerr << "Usage: " << argv[0] << " config.cfg" << std::endl;
		exit(0);
	}

	Bot sauerkraut(argv[1]);


	//Connection to client, start with the client's host, (how cube does it)
	sauerkraut.host_create();
	
	sauerkraut.event_loop();

	sauerkraut.host_clearup();
}
