#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>
#include <inttypes.h>
#include <signal.h>
#include <enet/enet.h>
#include <libconfig.h++>
#include "bot.h"
#include "handler.h"


using namespace libconfig;

Bot::Bot(char *filename){
	Config cfg;
	try{
		cfg.readFile(filename);
	} catch(const FileIOException &fioex){
		std::cerr << "I/O error while reading file." << std::endl;
	} catch(const ParseException &pex){
		std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()	<< " - " << pex.getError() << std::endl;
	}


	// Read Config 
	std::string config_name;
	std::string config_addr;
	unsigned int config_port;
	config_name = cfg.lookup("name").c_str();
	config_addr= cfg.lookup("server").c_str();
	config_port = cfg.lookup("port");

	name = config_name;
	server = config_addr;
	port = config_port;
}

Bot::~Bot(){

	// Clean up anything

}


int Bot::host_create(){
	enet_address_set_host(&address,server.c_str());
	address.port = port;
	
	std::cout << "Trying to connect to: " << server.c_str() << ":" << port << std::endl;

	clienthost = enet_host_create(NULL,2,3,0,0);
	if(clienthost){
		connpeer = enet_host_connect(clienthost, &address, 2, 0);
		enet_host_flush(clienthost);
	} else {
		std::cerr << "Could not connect to server" << std::endl;
		return EXIT_FAILURE;
	}

	if(!clienthost){
		std::cerr << "Bad clienthost?" << std::endl;
		return EXIT_FAILURE;
	}

	return(0);
}

void Bot::event_loop(){
	while(clienthost){
	while((enet_host_service(clienthost, &event, 0)) > 0 ){
		switch(event.type) {
			case ENET_EVENT_TYPE_CONNECT:
				std::cout << "Connected to server" << std::endl;
				break;
			case ENET_EVENT_TYPE_RECEIVE:
				if (event.channelID == 1){
					//std::cout << event.packet->data << std::endl;
					char *datarecv;
					size_t packetlength = event.packet->dataLength;
					datarecv = (char *)malloc(packetlength);
					std::memcpy(datarecv, event.packet->data,packetlength);
					enet_packet_destroy (event.packet);
					//Handler Function
					datahandler(datarecv,packetlength,connpeer);
					enet_host_flush(clienthost);
					free(datarecv);
				} else {
					//std::cerr << "Data over another channel: " << event.channelID << std::endl;
				}
				break;
			case ENET_EVENT_TYPE_DISCONNECT:
				std::cout << "Disconnect event" << std::endl;
				event.peer->data = NULL;
				enet_peer_reset (connpeer);
				clienthost = NULL;
				break;
			default: {
					 std::cout << "Another type of Event" << std::endl;
					 break;
				 }
		}
	}
	}
}

void Bot::host_clearup(){
	enet_host_destroy(clienthost);
}
